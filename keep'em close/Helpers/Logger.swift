//
//  Logger.swift
//  keep'em close
//
//  Created by Steve Gigou on 14/03/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import Foundation

class Logger {
  
  static func error(_ message: String) {
    debugPrint("[ERROR] " + message)
  }
  
  static func warning(_ message: String) {
    debugPrint("[WARNING] " + message)
  }
  
  static func info(_ message: String) {
    debugPrint("[INFO] " + message)
  }
  
  static func debug(_ message: String) {
    debugPrint("[DEBUG] " + message)
  }
  
}
