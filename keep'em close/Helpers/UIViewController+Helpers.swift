//
//  UIViewController+Helpers.swift
//  keep'em close
//
//  Created by Steve Gigou on 07/02/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import Foundation
import UIKit

public extension UIViewController {
  
  public func add(childViewController: UIViewController, inContainerView container: UIView) {
    addChildViewController(childViewController)
    container.addSubview(childViewController.view)
    childViewController.view.fitTo(container)
    childViewController.didMove(toParentViewController: self)
  }
  
  func pushToNavigation(_ viewController: UIViewController) {
    guard let navigationController = navigationController else {
      Logger.error("Could not push \(viewController) because is not in a navigation context")
      return
    }
    navigationController.pushViewController(viewController, animated: true)
  }
  
}
