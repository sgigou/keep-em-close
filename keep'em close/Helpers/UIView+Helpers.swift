//
//  UIView+Helpers.swift
//  keep'em close
//
//  Created by Steve Gigou on 07/02/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
  public func fitTo(_ view: UIView) {
    translatesAutoresizingMaskIntoConstraints = false
    topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
  }
}
