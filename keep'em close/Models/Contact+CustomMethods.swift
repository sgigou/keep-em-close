//
//  Contact+CustomMethods.swift
//  keep'em close
//
//  Created by Steve Gigou on 08/01/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import Foundation

extension Contact {
  func getFullName() -> String {
    if firstName == nil && lastName == nil {
      return "Unknown"
    }
    guard let firstName = firstName else {
      return lastName!
    }
    guard let lastName = lastName else {
      return firstName
    }
    return "\(firstName) \(lastName)"
  }
}
