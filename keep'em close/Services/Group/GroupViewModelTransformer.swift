//
//  GroupViewModelTransformer.swift
//  keep'em close
//
//  Created by Steve Gigou on 17/03/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import Foundation

class GroupViewModelTransformer {
  
  static func toViewModel(_ model: Group) -> GroupViewModel {
    let viewModel = GroupViewModel()
    update(viewModel, withModel: model)
    return viewModel
  }
  
  static func toModel(_ viewModel: GroupViewModel) -> Group {
    let model = GroupStore.entity()
    update(model, withViewModel: viewModel)
    return model;
  }
  
  static func update(_ model: Group, withViewModel viewModel: GroupViewModel) {
    model.name = viewModel.name
    model.frequency = viewModel.frequency
  }
  
  static func update(_ viewModel: GroupViewModel, withModel model: Group) {
    viewModel.name = model.name
    viewModel.frequency = model.frequency
  }
  
}
