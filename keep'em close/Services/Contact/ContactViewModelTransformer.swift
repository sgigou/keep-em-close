//
//  ContactViewModelTransformer.swift
//  keep'em close
//
//  Created by Steve Gigou on 07/02/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import Foundation

class ContactViewModelTransformer {
  
  static func toModel(_ viewModel: ContactViewModel) -> Contact {
    let contact = ContactStore.entity()
    update(contact, withViewModel: viewModel)
    return contact
  }
  
  static func toViewModel(_ contact: Contact) -> ContactViewModel {
    let viewModel = ContactViewModel()
    viewModel.firstName = contact.firstName
    viewModel.lastName = contact.lastName
    viewModel.notes = contact.notes
    return viewModel
  }
  
  static func update(_ contact: Contact, withViewModel viewModel: ContactViewModel) {
    contact.lastName = viewModel.lastName
    contact.firstName = viewModel.firstName
    contact.notes = viewModel.notes
  }
  
}
