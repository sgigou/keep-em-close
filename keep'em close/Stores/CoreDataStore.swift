//
//  CoreDataStore.swift
//  keep'em close
//
//  Created by Steve Gigou on 08/01/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStore {
  static let persistentContainerName = "keep_em_close"
  
  fileprivate static let sharedInstance = CoreDataStore()
  
  fileprivate lazy var persistentContainer: NSPersistentContainer = {
    let container = NSPersistentContainer(name: CoreDataStore.persistentContainerName)
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
      if let error = error as NSError? {
        // TODO
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    })
    return container
  }()
  
  static var context: NSManagedObjectContext {
    return sharedInstance.persistentContainer.viewContext
  }
  
  // MARK: Context
  
  fileprivate func saveContext() {
    let context = persistentContainer.viewContext
    if context.hasChanges {
      do {
        try context.save()
      } catch {
        let nserror = error as NSError
        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
      }
    }
  }
  
  public static func persistContext() {
    CoreDataStore.sharedInstance.saveContext()
  }
  
  public static func object<T:NSManagedObject> () -> T {
    return T(context: CoreDataStore.sharedInstance.persistentContainer.viewContext)
  }
  
  public static func delete(_ object: NSManagedObject) {
    sharedInstance.persistentContainer.viewContext.delete(object)
  }
  
  // MARK: Request
  
  public static func perform<T>(fetchRequest: String) -> T? {
    let container = sharedInstance.persistentContainer
    guard let request = container.managedObjectModel.fetchRequestTemplate(forName: fetchRequest) else {
      return nil
    }
    do {
      let res = try container.viewContext.execute(request) as? NSAsynchronousFetchResult<NSFetchRequestResult>
      return res?.finalResult as? T
    }
    catch {
      debugPrint("Failed to execute \(fetchRequest): \(error)")
      return nil
    }
  }
  
}
