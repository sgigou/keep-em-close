//
//  ContactStore.swift
//  keep'em close
//
//  Created by Steve Gigou on 06/01/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import Foundation
import CoreData

class ContactStore {
  
  // MARK: Context
  
  static func entity() -> Contact {
    let contact: Contact = CoreDataStore.object()
    return contact
  }
  
  static func persist() {
    CoreDataStore.persistContext()
  }
  
  static func delete(_ contact: Contact) {
    CoreDataStore.delete(contact)
  }
  
  // MARK: Requests
  
  static func findAll() -> [Contact] {
    let request = NSFetchRequest<Contact>(entityName: "Contact")
    request.sortDescriptors = [
      NSSortDescriptor(
        key: #keyPath(Contact.lastName),
        ascending: true,
        selector: #selector(NSString.caseInsensitiveCompare)
      ),
      NSSortDescriptor(
        key: #keyPath(Contact.firstName),
        ascending: true,
        selector: #selector(NSString.caseInsensitiveCompare)
      )
    ]
    do {
      return try CoreDataStore.context.fetch(request)
    } catch {
      print("Could not fetch contacts")
      return []
    }
  }
}

