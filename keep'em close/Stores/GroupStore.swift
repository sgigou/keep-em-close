//
//  GroupStore.swift
//  keep'em close
//
//  Created by Steve Gigou on 17/03/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import Foundation
import CoreData

class GroupStore {
  
  static let entityName = "Group"
  
  static func entity() -> Group {
    return CoreDataStore.object()
  }
  
  static func persist() {
    CoreDataStore.persistContext()
  }
  
  static func delete(_ group: Group) {
    CoreDataStore.delete(group)
  }
  
}

// MARK: - Fetch requests

extension GroupStore {
  
  static func findAll() -> [Group] {
    let request = NSFetchRequest<Group>(entityName: GroupStore.entityName)
    request.sortDescriptors = [
      NSSortDescriptor(key: #keyPath(Group.name), ascending: true, selector: #selector(NSString.caseInsensitiveCompare))
    ]
    do {
      return try CoreDataStore.context.fetch(request)
    } catch {
      Logger.error("Could not execute findAll in GroupStore")
      return []
    }
  }
  
}
