//
//  ContactViewModel.swift
//  keep'em close
//
//  Created by Steve Gigou on 07/02/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import Foundation

class ContactViewModel {
  
  var firstName: String?
  var lastName: String?
  var notes: String?
  
}
