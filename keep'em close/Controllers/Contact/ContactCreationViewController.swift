//
//  ContactCreationViewController.swift
//  keep'em close
//
//  Created by Steve Gigou on 15/01/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class ContactCreationViewController: UIViewController {
  
  @IBOutlet weak var formView: UIScrollView!
  
  let contactFormViewController = ContactFormViewController()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    add(childViewController: contactFormViewController, inContainerView: formView)
  }

  @IBAction func onSaveAction(_ sender: UIBarButtonItem) {
    _ = ContactViewModelTransformer.toModel(contactFormViewController.displayedContact)
    ContactStore.persist()
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func onCancelAction(_ sender: UIBarButtonItem) {
    dismiss(animated: true, completion: nil)
  }
  
}
