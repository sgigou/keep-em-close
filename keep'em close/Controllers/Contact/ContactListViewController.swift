//
//  ContactListViewController.swift
//  keep'em close
//
//  Created by Steve Gigou on 03/01/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class ContactListViewController: UIViewController {

  @IBOutlet weak var contactsTableView: UITableView!
  
  var contacts: [Contact] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    contactsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Groups", style: .plain, target: self, action: #selector(groupsBarButtonTapped))
  }
  
  override func viewWillAppear(_ animated: Bool) {
    reloadData()
  }
  
  func reloadData() {
    contacts = ContactStore.findAll()
    contactsTableView.reloadData()
  }
  
  func delete(atIndexPath indexPath: IndexPath) {
    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    alert.addAction(UIAlertAction(title: "Confirm deletion", style: .destructive, handler: {
      alert in
      let contact = self.contacts[indexPath.row]
      ContactStore.delete(contact)
      ContactStore.persist()
      self.contacts.remove(at: indexPath.row)
      self.contactsTableView.deleteRows(at: [indexPath], with: .fade)
    }))
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
      alert in
      debugPrint("Cancel selected")
    }))
    present(alert, animated: true)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ContactDetailsSegue" {
      let viewController = segue.destination as! ContactDisplayViewController
      guard let indexPath = self.contactsTableView.indexPathForSelectedRow else {
        return
      }
      viewController.initWithContact(contacts[indexPath.row])
    }
  }
  
  @objc private func groupsBarButtonTapped() {
    pushToNavigation(GroupsListViewController())
  }
  
}

// MARK: - UITableViewDataSource

extension ContactListViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return contacts.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let contact = contacts[indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactTableViewCell
    cell.initWithContact(contact)
    return cell
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      delete(atIndexPath: indexPath)
    }
  }
  
}
