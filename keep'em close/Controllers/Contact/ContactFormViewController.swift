//
//  ContactFormViewController.swift
//  keep'em close
//
//  Created by Steve Gigou on 17/01/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class ContactFormViewController: UIViewController {
  
  private(set) var displayedContact: ContactViewModel!
  
  @IBOutlet weak var lastNameTextField: UIFillingTextField!
  @IBOutlet weak var firstNameTextField: UIFillingTextField!
  @IBOutlet weak var notesTextView: UITextView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    lastNameTextField.valueChangedValid = {
      newValue, textField in
      self.displayedContact.lastName = textField.text
    }
    firstNameTextField.valueChangedValid = {
      newValue, textField in
      self.displayedContact.firstName = textField.text
    }
    notesTextView.delegate = self
    
    if displayedContact == nil {
      displayedContact = ContactViewModel()
    }
    reload()
  }
  
  func initWithContact(contact: ContactViewModel) {
    displayedContact = contact
  }
  
  func reload() {
    lastNameTextField.text = displayedContact.lastName
    firstNameTextField.text = displayedContact.firstName
    notesTextView.text = displayedContact.notes
  }
}

extension ContactFormViewController: UITextViewDelegate {
  func textViewDidChange(_ textView: UITextView) {
    self.displayedContact.notes = textView.text
  }
}
