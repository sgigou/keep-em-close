//
//  ContactDisplayViewController.swift
//  keep'em close
//
//  Created by Steve Gigou on 14/02/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class ContactDisplayViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  
  var contact: Contact!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 44
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    reload()
  }
  
  // MARK: - Data
  
  private func reload() {
    title = contact.getFullName()
    if tableView != nil {
      tableView.reloadData()
    }
  }
  
  func initWithContact(_ contact: Contact) {
    self.contact = contact
    reload()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard let identifier = segue.identifier else {
      Logger.error("No identifier in \(segue)")
      return
    }
    switch (identifier, segue.destination) {
    case ("editContactSegue", let editViewController as ContactEditViewController):
      editViewController.initWithContact(contact)
    default:
      Logger.error("prepare(for segue) \(identifier) \(segue.destination)")
    }
  }
  
}

// MARK: - UITableViewDataSource

extension ContactDisplayViewController: UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    if section == 0 {
      return ""
    }
    return "Events with " + self.contact.getFullName()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return 1
    }
    return 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == 0 {
      return generateContactCell(tableView, forIndexPath: indexPath)
    }
    return generateEventCell(tableView, forIndexPath: indexPath)
  }
  
  private func generateContactCell(_ tableView: UITableView, forIndexPath indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ContactDetailsCell", for: indexPath) as! ContactDetailsTableViewCell
    cell.initWithContact(self.contact)
    return cell
  }
  
  private func generateEventCell(_ tableView: UITableView, forIndexPath indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath)
    return cell
  }
  
}
