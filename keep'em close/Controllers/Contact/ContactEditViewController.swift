//
//  ContactEditViewController.swift
//  keep'em close
//
//  Created by Steve Gigou on 21/02/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class ContactEditViewController: UIViewController {
  
  @IBOutlet weak var formView: UIScrollView!
  
  private var contact: Contact!
  let contactFormViewController = ContactFormViewController()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    add(childViewController: contactFormViewController, inContainerView: formView)
  }
  
  // MARK: - Data
  
  func initWithContact(_ contact: Contact) {
    self.contact = contact
    let contactViewModel = ContactViewModelTransformer.toViewModel(contact)
    contactFormViewController.initWithContact(contact: contactViewModel)
  }
  
  // MARK: - Actions

  @IBAction func onCancelAction(_ sender: UIBarButtonItem) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func onSaveAction(_ sender: UIBarButtonItem) {
    guard let viewModel = contactFormViewController.displayedContact else {
      return
    }
    ContactViewModelTransformer.update(contact, withViewModel: viewModel)
    ContactStore.persist()
    dismiss(animated: true, completion: nil)
  }
  
}
