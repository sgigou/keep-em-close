//
//  GroupCreationViewController.swift
//  keep'em close
//
//  Created by Steve Gigou on 15/03/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class GroupCreationViewController: UIViewController {
  
  @IBOutlet weak var formScrollView: UIScrollView!
  
  let groupFormViewController = GroupFormViewController(withGroup: nil)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    add(childViewController: groupFormViewController, inContainerView: formScrollView)
  }
  
  @IBAction func cancelBarButtonTapped(_ sender: UIBarButtonItem) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func saveBarButtonTapped(_ sender: UIBarButtonItem) {
    _ = GroupViewModelTransformer.toModel(groupFormViewController.group)
    GroupStore.persist()
    dismiss(animated: true, completion: nil)
  }
  
}
