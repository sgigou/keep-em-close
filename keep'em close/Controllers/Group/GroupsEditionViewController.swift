//
//  GroupsEditionViewController.swift
//  keep'em close
//
//  Created by Steve Gigou on 09/04/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class GroupsEditionViewController: UIViewController {
  
  @IBOutlet weak var formScrollView: UIScrollView!
  
  let group: Group
  let formViewController: GroupFormViewController
  
  init(withGroup group: Group) {
    self.group = group
    
    let groupViewModel = GroupViewModelTransformer.toViewModel(group)
    self.formViewController = GroupFormViewController(withGroup: groupViewModel)
    
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    self.group = Group()
    
    self.formViewController = GroupFormViewController(withGroup: GroupViewModel())
    
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    add(childViewController: formViewController, inContainerView: formScrollView)
  }
  
  @IBAction func onCancelBarButtonTap(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func onSaveBarButtonTap(_ sender: Any) {
    let groupViewModel = formViewController.group
    GroupViewModelTransformer.update(group, withViewModel: groupViewModel)
    GroupStore.persist()
    dismiss(animated: true, completion: nil)
  }
  
}
