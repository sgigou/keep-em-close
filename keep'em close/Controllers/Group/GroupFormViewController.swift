//
//  GroupFormViewController.swift
//  keep'em close
//
//  Created by Steve Gigou on 16/03/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class GroupFormViewController: UIViewController {
  
  private(set) var group: GroupViewModel
  
  @IBOutlet weak var nameTextField: UIFillingTextField!
  @IBOutlet weak var frequencyTextField: UIFillingTextField!
  
  init(withGroup group: GroupViewModel?) {
    self.group = group ?? GroupViewModel()
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    self.group = GroupViewModel()
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    nameTextField.valueChangedValid = {
      newValue, _ in
      self.group.name = newValue
    }
    frequencyTextField.valueChangedValid = {
      newValue, _ in
      self.group.frequency = newValue
    }
    reload()
  }
  
  private func reload() {
    nameTextField.text = group.name
    frequencyTextField.text = group.frequency
  }
  
}
