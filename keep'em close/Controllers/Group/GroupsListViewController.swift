//
//  GroupsListViewController.swift
//  keep'em close
//
//  Created by Steve Gigou on 15/03/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class GroupsListViewController: UIViewController {
  
  @IBOutlet weak var groupsTableView: UITableView!
  
  var firstAppear = true
  var groups: [Group] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    displayNavigationBar()
    
    groupsTableView.register(UINib.init(nibName: "GroupTableViewCell", bundle: nil), forCellReuseIdentifier: "GroupCell")
    groupsTableView.rowHeight = GroupTableViewCell.rowHeight
    
    fetchData(andReload: false)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    if firstAppear {
      firstAppear = false
      return
    }
    fetchData()
  }
  
  private func displayNavigationBar() {
    title = "Groups"
    navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
  }
  
  @objc private func addTapped() {
    guard let navigationController = navigationController else {
      Logger.error("GroupsListViewController has no navigation controller")
      return
    }
    navigationController.present(GroupCreationViewController(), animated: true, completion: nil)
  }
  
}

// MARK: - Data

extension GroupsListViewController {
  
  private func fetchData(andReload: Bool = true) {
    groups = GroupStore.findAll()
    if andReload {
      groupsTableView.reloadData()
    }
  }
  
  func delete(atIndexPath indexPath: IndexPath) {
    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    alert.addAction(UIAlertAction(title: "Confirm deletion", style: .destructive, handler: {
      alert in
      let group = self.groups[indexPath.row]
      GroupStore.delete(group)
      GroupStore.persist()
      self.groups.remove(at: indexPath.row)
      self.groupsTableView.deleteRows(at: [indexPath], with: .fade)
    }))
    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
      alert in
      debugPrint("Cancel selected")
    }))
    present(alert, animated: true)
  }
  
}

// MARK: - UITableViewDataSource

extension GroupsListViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return groups.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCell", for: indexPath) as! GroupTableViewCell
    cell.displayGroup(groups[indexPath.row])
    return cell
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      delete(atIndexPath: indexPath)
    }
  }
  
}

// MARK: - UITableViewDelegate

extension GroupsListViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let navigationController = navigationController else {
      Logger.error("GroupsListViewController has no navigation controller")
      return
    }
    let group = groups[indexPath.row]
    navigationController.present(GroupsEditionViewController(withGroup: group), animated: true, completion: nil)
  }
  
}
