//
//  ContactCellTableViewCell.swift
//  keep'em close
//
//  Created by Steve Gigou on 07/02/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func initWithContact(_ contact: Contact) {
    textLabel?.text = contact.getFullName()
    detailTextLabel?.text = "5 days"
  }
  
}
