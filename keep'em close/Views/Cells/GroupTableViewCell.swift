//
//  GroupTableViewCell.swift
//  keep'em close
//
//  Created by Steve Gigou on 19/03/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {
  
  static let rowHeight: CGFloat = 62
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var frequencyLabel: UILabel!
  @IBOutlet weak var contactsLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func displayGroup(_ group: Group) {
    nameLabel.text = group.name
    frequencyLabel.text = group.frequency
    if let contacts = group.contacts {
      let contactsWord = contacts.count == 1 ? "contact" : "contacts"
      contactsLabel.text = "\(contacts.count) \(contactsWord)"
    } else {
      contactsLabel.text = "0 contacts"
    }
  }
  
}
