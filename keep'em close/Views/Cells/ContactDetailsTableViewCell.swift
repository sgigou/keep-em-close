//
//  ContactDetailsTableViewCell.swift
//  keep'em close
//
//  Created by Steve Gigou on 21/02/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class ContactDetailsTableViewCell: UITableViewCell {
  
  @IBOutlet weak var detailsLabel: UILabel!
  
  func initWithContact(_ contact: Contact) {
    detailsLabel.text = contact.notes ?? "No notes"
  }
  
}
