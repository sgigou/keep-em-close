//
//  UIFillingTextField.swift
//  keep'em close
//
//  Created by Steve Gigou on 21/01/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class UIFillingTextField: UITextField {
  
  let border = CALayer()
  
  var validator: (String) -> Bool = {
    value in
    return true
  }
  var valueChangedValid: (String, UIFillingTextField) -> Void = {
    newValue, textField in
    debugPrint("New valid value \(newValue)")
    textField.setValid()
  }
  var valueChangedInvalid: (String, UIFillingTextField) -> Void = {
    newValue, textField in
    debugPrint("New invalid value \(newValue)")
    textField.setInvalid()
  }
  
  // MARK: Init
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  func commonInit() {
    self.addTarget(self, action: #selector(valueChanged), for: .editingChanged)
  }
  
  // MARK: Display
  
  override func draw(_ rect: CGRect) {
    let width = CGFloat(1.0)
    border.borderColor = UIColor.lightGray.cgColor
    border.frame = CGRect(x: 0, y: frame.size.height - width, width:  frame.size.width, height: frame.size.height)
    border.borderWidth = width
    layer.addSublayer(border)
    layer.masksToBounds = true
  }
  
  func setValid() {
    border.borderColor = UIColor.lightGray.cgColor
  }
  
  func setInvalid() {
    border.borderColor = UIColor.red.cgColor
  }
  
  // MARK: Actions
  
  @objc func valueChanged() {
    let value = text ?? ""
    if validator(value) {
      valueChangedValid(value, self)
    } else {
      valueChangedInvalid(value, self)
    }
  }
}
