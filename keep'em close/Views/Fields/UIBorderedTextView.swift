//
//  UIBorderedTextView.swift
//  keep'em close
//
//  Created by Steve Gigou on 22/02/2018.
//  Copyright © 2018 Novelia. All rights reserved.
//

import UIKit

class UIBorderedTextView: UITextView {
  
  let border = CALayer()
  
  override func draw(_ rect: CGRect) {
    setHeight()
    drawBottomBorder()
  }
  
  private func drawBottomBorder() {
    let width = CGFloat(1.0)
    border.borderColor = UIColor.lightGray.cgColor
    border.frame = CGRect(x: 0, y: 0, width: width, height: frame.size.height)
    border.borderWidth = width
    layer.addSublayer(border)
    layer.masksToBounds = true
  }
  
  private func setHeight() {
    heightAnchor.constraint(equalToConstant: 40).isActive = true
  }
  
}
